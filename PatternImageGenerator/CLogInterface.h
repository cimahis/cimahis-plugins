#ifndef CLOGINTERFACE_H
#define CLOGINTERFACE_H

#include <QString>

class CLogInterface
{
signals:
    virtual void log(QString str) = 0;
public:
    virtual ~CLogInterface(){}
};

Q_DECLARE_INTERFACE(CLogInterface, "org.cimahis.CLogInterface/1.0.0")

#endif // CLOGINTERFACE_H
