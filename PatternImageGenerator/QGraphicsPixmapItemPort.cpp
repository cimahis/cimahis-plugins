#include "QGraphicsPixmapItemPort.h"
#include <QPen>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsRectItem>
#include <QCursor>
#include <limits>
#include "QGraphicsItemPort.h"
#include "ROISelectionDialog.h"
QGraphicsPixmapItemPort::QGraphicsPixmapItemPort(const QPixmap& im,
        QGraphicsItem* parent) :
    QObject(0), QGraphicsPixmapItem(im, parent)
{
    bordeCuadro = 0;
    refPto1 = refPto2 = QPointF();
    cuadrosColorRegular = Qt::yellow;
    cuadrosColorResaltado = Qt::red;
    anchoLineaCuadros = 1.;
    lineaExternaCuadros = 6.;
    haySeleccion = false;
}

QGraphicsPixmapItemPort::QGraphicsPixmapItemPort(ROISelectionDialog* ventana,
        const QPixmap& im) :
    QGraphicsPixmapItemPort(im)
{
    dialogo = ventana;
}

QGraphicsPixmapItemPort::~QGraphicsPixmapItemPort()
{
    limpiarCuadros();
    delete bordeCuadro;
}


QPixmap QGraphicsPixmapItemPort::getImagen() const
{
    return pixmap();
}


void QGraphicsPixmapItemPort::setSeleccion(bool marcado)
{
    haySeleccion = marcado;

    for (QGraphicsItemPort*  b : cuadros)
    {
        b->setEnabled( !marcado );
    }

    if (marcado)
    {
        setCursor( Qt::CrossCursor );
    }

    else
    {
        unsetCursor();
    }
}

void QGraphicsPixmapItemPort::setImagen(const QPixmap& im)
{
    limpiarCuadros();
    delete bordeCuadro;
    setPixmap( im );
}


void QGraphicsPixmapItemPort::agregarCuadro(const QRectF& cuadro)
{
    QString label = QObject::tr("%1").arg(cuadros.count() + 1);
    QGraphicsItemPort* newBox = new QGraphicsItemPort(cuadro, label,
            anchoLineaCuadros,
            cuadrosColorRegular,
            cuadrosColorResaltado,
            lineaExternaCuadros,
            this);
    cuadros.append(newBox);
    emit cuadroAgregado( newBox );
    emit areaSeleccionada();
}


void QGraphicsPixmapItemPort::eliminarCuadro(QGraphicsItemPort* b)
{
    b->setParentItem(0);
    cuadros.removeOne( b );
    delete b;
}

QList<QPixmap*>* QGraphicsPixmapItemPort::getListaPixmap(QPixmap pixmap)
{
    qreal w = pixmap.width();
    qreal h = pixmap.height();
    QTransform general;
    QPointF p[4];
    p[0] = QPoint(0, 0);
    p[1] = QPointF(w, 0);
    p[2] = QPointF(w, h);
    p[3] = QPointF(0, h);
    QPointF offsGen(0, 0);

    for (int i = 0; i < 4; ++i)
    {
        QPointF tempGen = general.map(p[i]);
        offsGen.setX(qMin(offsGen.x(), tempGen.x()));
        offsGen.setY(qMin(offsGen.y(), tempGen.y()));
    }

    QList<QPixmap*>* listaPixmap = new QList<QPixmap*>;

    foreach (QGraphicsItemPort* e, cuadros)
    {
        QPointF LTgen = general.map(e->mapToScene(e->referenceRect().topLeft())) -
                        offsGen;
        int Xgen = static_cast<int>(LTgen.x());
        int Ygen = static_cast<int>(LTgen.y());
        qreal anchoCuadro = e->referenceRect().width();
        qreal altoCuadro = e->referenceRect().height();
        QPixmap* cuadroPixmap = new QPixmap(pixmap.transformed(general,
                                            Qt::SmoothTransformation).
                                            copy(Xgen, Ygen, anchoCuadro, altoCuadro));
        listaPixmap->append(cuadroPixmap);
    }

    return listaPixmap;
}


void QGraphicsPixmapItemPort::limpiarCuadros()
{
    for (QGraphicsItemPort* box : cuadros)
    {
        delete box;
    }

    cuadros.clear();
}



void QGraphicsPixmapItemPort::limpiarSeleccion()
{
    for (QGraphicsItemPort* box : cuadros)
    {
        box->setSelected( false );
    }
}


void QGraphicsPixmapItemPort::resaltarCuadro(QString nombreCuadro)
{
    for (QGraphicsItemPort* box : cuadros)
    {
        box->setSelected(box->getEtiqueta().trimmed().contains(nombreCuadro.trimmed()));
    }
}
void QGraphicsPixmapItemPort::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    if ( boundingRect().contains( event->pos() ) )
    {
        refPto1 = event->pos();

        if (haySeleccion)
        {
            bordeCuadro = new QGraphicsRectItem( QRectF(refPto1, QSizeF(0, 0)) );
            bordeCuadro->setPen( QPen(Qt::white, anchoLineaCuadros, Qt::DotLine) );
            bordeCuadro->setEnabled( false );
            bordeCuadro->setParentItem( this );
        }
    }
}


void QGraphicsPixmapItemPort::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
    if (!bordeCuadro)
    {
        return;
    }

    if ( boundingRect().contains( event->pos() ) )
    {
        refPto2 = event->pos();

        if (haySeleccion)
        {
            normalizarAArea(refPto2 = event->pos());
            qreal left = qMin(refPto1.x(), refPto2.x());
            qreal top  = qMin(refPto1.y(), refPto2.y());
            qreal width  = qAbs(refPto1.x() - refPto2.x());
            qreal height = qAbs(refPto1.y() - refPto2.y());
            bordeCuadro->setRect(left, top, width, height);
        }
    }
}


void QGraphicsPixmapItemPort::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
    Q_UNUSED( event )

    if (haySeleccion && bordeCuadro)
    {
        agregarCuadro( bordeCuadro->rect() );
        cuadros.last()->setEnabled( false );
        delete bordeCuadro;
        bordeCuadro = 0;
        refPto1 = refPto2 = QPointF();
    }
}


void QGraphicsPixmapItemPort::normalizarAArea(QPointF& pt)
{
    qreal w = boundingRect().width();
    qreal h = boundingRect().height();
    pt.setX( qMax(pt.x(), 0.) );
    pt.setX( qMin(pt.x(), w) );
    pt.setY( qMax(pt.y(), 0.) );
    pt.setY( qMin(pt.y(), h) );
}
