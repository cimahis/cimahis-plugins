#ifndef CIMAGEINTERFACE_H
#define CIMAGEINTERFACE_H

#include <QImage>

class CImageInterface
{
signals:
    virtual void mostrarImagen(QImage currentImage, QString imageFilename) = 0;
public:
    virtual ~CImageInterface(){}
};

Q_DECLARE_INTERFACE(CImageInterface, "org.cimahis.CImageInterface/1.0.0")

#endif // CIMAGEINTERFACE_H
