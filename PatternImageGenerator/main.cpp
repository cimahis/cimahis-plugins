#include <QApplication>
#include "ROISelectionDialog.h"
int main(int argc, char* argv[])
{
    QApplication a(argc, argv);
    QImage img;
    ROISelectionDialog rsd(img);
    rsd.setVisible(true);
    return a.exec();
}
