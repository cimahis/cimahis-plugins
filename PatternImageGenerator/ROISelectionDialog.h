#ifndef ROISELECTIONDIALOG_H
#define ROISELECTIONDIALOG_H
#include <QGraphicsScene>
#include <QDialog>
#include <QPixmap>
#include <QList>
#include <QImage>
#include <QPaintEvent>
#include "QGraphicsPixmapItemPort.h"

#include <QObject>
#include "CImageInterface.h"
#include "CLogInterface.h"
#include "CStatusBarInterface.h"

#include <QPainter>
#include <QPen>
#include <QDebug>
#include <QListWidgetItem>
#include <QFileDialog>
namespace Ui
{
    class ROISelectionDialog;
}

class ROISelectionDialog : public QDialog, CImageInterface,
    CLogInterface, CStatusBarInterface
{
        Q_OBJECT
        Q_INTERFACES(CImageInterface)
        Q_INTERFACES(CLogInterface)
        Q_INTERFACES(CStatusBarInterface)
    public:
        ROISelectionDialog();
        explicit ROISelectionDialog(QWidget* parent);
        ROISelectionDialog(QImage imagen );
        ROISelectionDialog(QImage imagen, QString nombre_imagen);
        ~ROISelectionDialog();
        QImage getImagenPatron();
        QString getNombreImagenPatron();
    public slots:
        void seleccionarRois(QImage imagen);
        void seleccionarRois(QImage imagen, QString nombre);
    private slots:
        void guardarReferencias(QGraphicsItemPort* nuevoElemento);
        void actualizarGui();
        void on_buttonGenerate_clicked();
        void actualizarTagAmpliacion();
        void on_buttonCancel_clicked();
        void on_buttonDeleteArea_clicked();
        void on_buttonZoomIn_clicked();
        void on_buttonZoomOut_clicked();
        void on_buttonSelectArea_toggled(bool checked);
        void on_areaList_currentRowChanged(int currentRow);
        void on_entryZoomStep_editingFinished();
        void on_buttonSave_clicked();

    private:
        void setNombreArchivo(QString nombre);
        void setImagen(QImage imagen);
        void conectarElementos();

        qreal escalaActual;
        qreal pasoZoom;
        QList<QPixmap*>* listaDePixmap;
        QList<QLabel*>* listaDeEtiquetas;
        QGraphicsPixmapItemPort* elementoPixmap;
        QGraphicsScene* areaDeImagen;
        QPixmap pixmapOriginal;
        QImage imagenGenerada;
        QImage* ultimaImagenPatron;
        const QString filter = "Image Files (*.png *.jpg *.bmp)";
        Ui::ROISelectionDialog* ui;
        const QString prefijo = "ROI";
        void crearEtiquetasPixmap(QList<QPixmap*>* listaPixmap);
        void limpiarListaPixmap();
        void limpiarListaEtiquetasPixmap();
        void limpiarVistaPrevia();
        int* calcularDimensionesDeImagen(int longitud);
        QList<QRgb>* getListaRgb(QList<QPixmap*>* listaPixmap);
        QImage* getImagenPatron(QList<QRgb> listaPixel, int ancho, int alto);
        void setVistaPrevia( QPixmap pm, int ancho, int alto, int noPixeles);
        void setVisibleVistaPrevia(bool visible);
        QString ubicacionActual;
        QString nombreOriginal;
        QString nombreImagenGenerada;
        bool refrescarMiniaturas();
        void refrescarVistaPrevia();
        QString getNombreNuevo(QString nombreImagen);

    signals:
        void mostrarImagen(QImage currentImage, QString imageFilename);
        void log(QString str);
        void setStatus(QString str, int timeout);
        void eliminarCuadroDeEscena(QGraphicsItemPort* item);

};

#endif // ROISELECTIONDIALOG_H
