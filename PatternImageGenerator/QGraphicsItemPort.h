#ifndef QGRAPHICSITEMPORT_H
#define QGRAPHICSITEMPORT_H
#include <QGraphicsItem>
#include <QStaticText>
#include <QLabel>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QCursor>
#include <QVariant>
class QGraphicsItemPort : public QGraphicsItem
{
        //*!
    public:
        explicit QGraphicsItemPort(const QRectF& refRect = QRectF(),
                                   const QString& getEtiqueta = QString(),
                                   qreal getAnchoLineaCuadro = 1.,
                                   const QColor& getColorRegular = Qt::yellow,
                                   const QColor& getColorResaltado = Qt::green,
                                   qreal outlyingCoverage = 0.,
                                   QGraphicsItem* parent = 0);

        QRectF referenceRect() const;
        void setReferenceRect(const QRectF& r);
        QRectF boundingRect() const;
        void paint(QPainter* painter,
                   const QStyleOptionGraphicsItem* option,
                   QWidget* widget);

        QString getEtiqueta() const;
        void setEtiqueta(const QString& l);
        void mostrarEtiqueta(bool show);

        QColor getColorRegular() const;
        void setColorRegular(const QColor& c);

        QColor getColorResaltado() const;
        void setColorResaltado(const QColor& c);

        qreal getAnchoLineaCuadro() const;
        void setAnchoLineaCuadro(qreal w);

        qreal outlyingCoverage() const;
        void setOutlyingCoverage(qreal r);

    protected:
        void hoverEnterEvent(QGraphicsSceneHoverEvent* event);
        void hoverLeaveEvent(QGraphicsSceneHoverEvent* event);
        QVariant itemChange(GraphicsItemChange change, const QVariant& value);
        void hoverMoveEvent(QGraphicsSceneHoverEvent* event);
        void mousePressEvent(QGraphicsSceneMouseEvent* event);
        void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
        void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);

        QStaticText etiqueta;
        bool etiquetaEsMostrada;

        QColor colorNormal;
        QColor colorResaltado;
        QColor colorActual;

        qreal anchoLineaCuadro;
        qreal coverturaExterna;
    private:
        enum BoxHandgripFlag
        {
            NO_HGRIP,
            TOP_LEFT_HGRIP,
            TOP_MIDDLE_HGRIP,
            TOP_RIGHT_HGRIP,
            BOTTOM_LEFT_HGRIP,
            BOTTOM_MIDDLE_HGRIP,
            BOTTOM_RIGHT_HGRIP,
            LEFT_MIDDLE_HGRIP,
            RIGHT_MIDDLE_HGRIP
        };

        void setCursorByHgripFlag(BoxHandgripFlag f);
        qreal distance(QPointF pt1, QPointF pt2) const;
        void truncToParentSize(QPointF& pt);

        QRectF refCuadro;
        QPointF refCuadroArriba;
        QPointF refCuadroAbajo;
        QPointF refCuadroIzquierda;
        QPointF refCuadroDerecha;

        BoxHandgripFlag banderaControl;
        bool redimensionadoEnProgreso;
        QRectF redimensionCuadro;
};

#endif  // QGRAPHICSITEMPORT_H
