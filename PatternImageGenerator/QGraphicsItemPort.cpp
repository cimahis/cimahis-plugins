#include "QGraphicsItemPort.h"
#include <QDebug>
QGraphicsItemPort::QGraphicsItemPort(const QRectF& refRect,
                                     const QString& label,
                                     qreal penWidth,
                                     const QColor& regularColor,
                                     const QColor& highLightColor,
                                     qreal outlyingCoverage,
                                     QGraphicsItem* parent) : QGraphicsItem( parent )
{
    etiqueta = QStaticText(label);
    etiquetaEsMostrada = true;
    colorNormal = regularColor;
    colorResaltado = highLightColor;
    colorActual = regularColor;
    anchoLineaCuadro = penWidth;
    coverturaExterna = outlyingCoverage;
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    setAcceptHoverEvents( true );
    setAcceptedMouseButtons( Qt::LeftButton );
    banderaControl = NO_HGRIP;
    redimensionadoEnProgreso = false;
    setReferenceRect( refRect );
}


QRectF QGraphicsItemPort::referenceRect() const
{
    return refCuadro;
}


void QGraphicsItemPort::setReferenceRect(const QRectF& r)
{
    if (refCuadro != r)
    {
        prepareGeometryChange();
        refCuadro = r.isValid() ? r : r.normalized();
        refCuadroArriba = QPointF(refCuadro.center().x(), refCuadro.top());
        refCuadroAbajo = QPointF(refCuadro.center().x(), refCuadro.bottom());
        refCuadroIzquierda = QPointF(refCuadro.left(), refCuadro.center().y());
        refCuadroDerecha = QPointF(refCuadro.right(), refCuadro.center().y());
        setToolTip( QObject::tr("w: %1px h:%2px").arg(
                        refCuadro.width()).arg(refCuadro.height()) );
    }
}

QString QGraphicsItemPort::getEtiqueta() const
{
    return etiqueta.text();
}

void QGraphicsItemPort::setEtiqueta(const QString& l)
{
    if (etiqueta.text() != l)
    {
        prepareGeometryChange();
    }

    etiqueta.setText(l);
}

void QGraphicsItemPort::mostrarEtiqueta(bool show)
{
    if (show)
    {
        prepareGeometryChange();
    }

    etiquetaEsMostrada = show;
}

QRectF QGraphicsItemPort::boundingRect() const
{
    QRectF baseRect;

    if (redimensionadoEnProgreso)
    {
        baseRect = (refCuadro | redimensionCuadro);
    }

    else
    {
        baseRect = refCuadro;
    }

    QRectF itemRect(baseRect.left(),
                    baseRect.top(),
                    baseRect.width() ,
                    baseRect.height());
    QRectF labelRect;

    if (etiquetaEsMostrada)
    {
        labelRect = QRectF(refCuadro.topLeft(), etiqueta.size());
    }

    return  (itemRect | labelRect);
}


void QGraphicsItemPort::paint(QPainter* painter,
                              const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    Q_UNUSED( option )
    Q_UNUSED( widget )
    QColor color = isEnabled() ? colorActual : colorActual.darker(150);
    painter->setPen( QPen(color, anchoLineaCuadro, Qt::DotLine) );
    painter->drawRect( refCuadro );
    painter->setPen( QPen(color, 4 * anchoLineaCuadro + 1, Qt::SolidLine) );

    if (etiquetaEsMostrada)
    {
        painter->drawStaticText(refCuadro.topLeft(), etiqueta);
    }

    if (redimensionadoEnProgreso)
    {
        painter->setPen( QPen(color.lighter(200), 1.0, Qt::SolidLine) );
        painter->drawRect( redimensionCuadro );
    }
}


void QGraphicsItemPort::hoverMoveEvent(QGraphicsSceneHoverEvent* event)
{
    qreal hgripsWidth = 2 * anchoLineaCuadro + 1;
    qreal hgripsCovergRad = hgripsWidth / 2 + coverturaExterna;
    QPointF pos = event->pos();

    if (distance(pos, refCuadro.topLeft()) <= hgripsCovergRad)
    {
        banderaControl = TOP_LEFT_HGRIP;
    }

    else if (distance(pos, refCuadro.topRight()) <= hgripsCovergRad)
    {
        banderaControl = TOP_RIGHT_HGRIP;
    }

    else if (distance(pos, refCuadro.bottomLeft()) <= hgripsCovergRad)
    {
        banderaControl = BOTTOM_LEFT_HGRIP;
    }

    else if (distance(pos, refCuadro.bottomRight()) <= hgripsCovergRad)
    {
        banderaControl = BOTTOM_RIGHT_HGRIP;
    }

    else if (distance(pos, refCuadroArriba) <= hgripsCovergRad)
    {
        banderaControl = TOP_MIDDLE_HGRIP;
    }

    else if (distance(pos, refCuadroAbajo) <= hgripsCovergRad)
    {
        banderaControl = BOTTOM_MIDDLE_HGRIP;
    }

    else if (distance(pos, refCuadroIzquierda) <= hgripsCovergRad)
    {
        banderaControl = LEFT_MIDDLE_HGRIP;
    }

    else if (distance(pos, refCuadroDerecha) <= hgripsCovergRad)
    {
        banderaControl = RIGHT_MIDDLE_HGRIP;
    }

    else
    {
        banderaControl = NO_HGRIP;
    }

    setCursorByHgripFlag( banderaControl );
}


void QGraphicsItemPort::hoverLeaveEvent(QGraphicsSceneHoverEvent* event)
{
    unsetCursor();

    if (!isSelected())
    {
        colorActual = colorNormal;
    }

    QGraphicsItem::hoverLeaveEvent( event );
}



void QGraphicsItemPort::hoverEnterEvent(QGraphicsSceneHoverEvent* event)
{
    if (!isSelected())
    {
        colorActual = colorResaltado;
    }

    QGraphicsItem::hoverEnterEvent( event );
}


void QGraphicsItemPort::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    if (banderaControl != NO_HGRIP)
    {
        redimensionadoEnProgreso = true;
        redimensionCuadro = refCuadro;
    }

    QGraphicsItem::mousePressEvent( event );
}


void QGraphicsItemPort::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
    if (redimensionadoEnProgreso)
    {
        prepareGeometryChange();
        QPointF pos;
        truncToParentSize(pos = event->pos());

        switch (banderaControl)
        {
            case NO_HGRIP:
                break;

            case TOP_LEFT_HGRIP:
                redimensionCuadro.setTopLeft( pos );
                break;

            case TOP_RIGHT_HGRIP:
                redimensionCuadro.setTopRight( pos );
                break;

            case BOTTOM_LEFT_HGRIP:
                redimensionCuadro.setBottomLeft( pos );
                break;

            case BOTTOM_RIGHT_HGRIP:
                redimensionCuadro.setBottomRight( pos );
                break;

            case TOP_MIDDLE_HGRIP:
                redimensionCuadro.setTop( pos.y() );
                break;

            case BOTTOM_MIDDLE_HGRIP:
                redimensionCuadro.setBottom( pos.y() );
                break;

            case LEFT_MIDDLE_HGRIP:
                redimensionCuadro.setLeft( pos.x() );
                break;

            case RIGHT_MIDDLE_HGRIP:
                redimensionCuadro.setRight( pos.x() );
                break;
        }
    }

    QGraphicsItem::mouseMoveEvent( event );
}


QColor QGraphicsItemPort::getColorRegular() const
{
    return colorNormal;
}


void QGraphicsItemPort::setColorRegular(const QColor& c)
{
    if (colorNormal != c)
    {
        if (colorActual == colorNormal)
        {
            colorActual = colorNormal = c;
            update();
        }

        else
        {
            colorNormal = c;
        }
    }
}

qreal QGraphicsItemPort::getAnchoLineaCuadro() const
{
    return anchoLineaCuadro;
}


void QGraphicsItemPort::setAnchoLineaCuadro(qreal w)
{
    if (anchoLineaCuadro != w)
    {
        prepareGeometryChange();
        anchoLineaCuadro = w;
    }
}


QColor QGraphicsItemPort::getColorResaltado() const
{
    return colorResaltado;
}


void QGraphicsItemPort::setColorResaltado(const QColor& c)
{
    if (colorResaltado != c)
    {
        if (colorActual == colorResaltado)
        {
            colorActual = colorResaltado = c;
            update();
        }

        else
        {
            colorResaltado = c;
        }
    }
}


qreal QGraphicsItemPort::outlyingCoverage() const
{
    return coverturaExterna;
}


void QGraphicsItemPort::setOutlyingCoverage(qreal r)
{
    if (coverturaExterna != r)
    {
        prepareGeometryChange();
        coverturaExterna = r;
    }
}

void QGraphicsItemPort::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
    if (redimensionadoEnProgreso)
    {
        setReferenceRect( redimensionCuadro );
        redimensionadoEnProgreso = false;
    }

    QGraphicsItem::mouseReleaseEvent(event);
}

void QGraphicsItemPort::setCursorByHgripFlag(BoxHandgripFlag f)
{
    switch (f)
    {
        case NO_HGRIP:
            unsetCursor();
            break;

        case TOP_LEFT_HGRIP:
        case BOTTOM_RIGHT_HGRIP:
            setCursor( Qt::SizeFDiagCursor );
            break;

        case TOP_RIGHT_HGRIP:
        case BOTTOM_LEFT_HGRIP:
            setCursor( Qt::SizeBDiagCursor );
            break;

        case TOP_MIDDLE_HGRIP:
        case BOTTOM_MIDDLE_HGRIP:
            setCursor( Qt::SizeVerCursor );
            break;

        case LEFT_MIDDLE_HGRIP:
        case RIGHT_MIDDLE_HGRIP:
            setCursor( Qt::SizeHorCursor );
            break;
    }
}


inline qreal QGraphicsItemPort::distance(QPointF pt1, QPointF pt2) const
{
    return QLineF(pt1, pt2).length();
}

void QGraphicsItemPort::truncToParentSize(QPointF& pt)
{
    if (scene())
    {
        qreal w = parentItem()->boundingRect().width();
        qreal h = parentItem()->boundingRect().height();
        pt.setX( qMax(pt.x(), 0.) );
        pt.setX( qMin(pt.x(), w) );
        pt.setY( qMax(pt.y(), 0.) );
        pt.setY( qMin(pt.y(), h) );
    }
}

QVariant QGraphicsItemPort::itemChange(GraphicsItemChange change,
                                       const QVariant& value)
{
    if (change == ItemSelectedChange)
    {
        bool selected = value.toBool();

        if (selected)
        {
            colorActual = colorResaltado;
        }

        else
        {
            colorActual = colorNormal;
        }
    }

    return QGraphicsItem::itemChange(change, value);
}
