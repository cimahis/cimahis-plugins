#ifndef QGRAPHICSPIXMAPITEMPORT_H
#define QGRAPHICSPIXMAPITEMPORT_H

#include <QObject>
#include <QGraphicsPixmapItem>
#include "QGraphicsItemPort.h"
#include <QPixmap>
#include <QMap>
class QGraphicsSceneMouseEvent;
class QGraphicsRectItem;
class ROISelectionDialog;
class QGraphicsPixmapItemPort : public QObject, public QGraphicsPixmapItem
{
        Q_OBJECT
    public:
        explicit QGraphicsPixmapItemPort(const QPixmap& im = QPixmap(),
                                         QGraphicsItem* dialogo = 0);
        QGraphicsPixmapItemPort(ROISelectionDialog* ventana,
                                const QPixmap& im = QPixmap());
        ~QGraphicsPixmapItemPort();
        QPixmap getImagen() const;
        QSizeF minBoxSize() const;
        QSizeF maxBoxSize() const;
        QSizeF avgBoxSize() const;
        QList<QPixmap*>* getListaPixmap(QPixmap pixmap);
        void setImagen(const QPixmap& im);
        void agregarElemento(QGraphicsItem* item);
        void agregarCuadro(const QRectF& cuadro);
        void limpiarCuadros();
        void setSeleccion(bool marcado);

    protected:
        void mousePressEvent(QGraphicsSceneMouseEvent* event);
        void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
        void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);

    public slots:
        void eliminarCuadro(QGraphicsItemPort* b);

    private:
        bool haySeleccion;
        QMap<QGraphicsItemPort*, QRectF*> mapaDeCuadros;
        QList<QGraphicsItemPort*> cuadros;
        QColor cuadrosColorRegular;
        QColor cuadrosColorResaltado;
        qreal anchoLineaCuadros;
        qreal lineaExternaCuadros;
        QGraphicsRectItem* bordeCuadro;
        QPointF refPto1, refPto2;
        QMap<QString, QString>* contexto;
        ROISelectionDialog* dialogo;

        void resaltarCuadro(QString nombreCuadro);
        void limpiarSeleccion();
        void normalizarAArea(QPointF& pt);

    signals:
        void cuadroAgregado(QGraphicsItemPort* b);
        void areaSeleccionada();

};

#endif  // QGRAPHICSPIXMAPITEMPORT_H
