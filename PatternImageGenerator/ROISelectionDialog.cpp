#include "ROISelectionDialog.h"
#include "ui_ROISelectionDialog.h"
#include <QMessageBox>
#include <QRgb>
#include <cmath>
#include <sstream>
#include <QDesktopServices>
#include <QtGui>
#include <iostream>
ROISelectionDialog::ROISelectionDialog() :
    ui(new Ui::ROISelectionDialog)
{
    ultimaImagenPatron = NULL;
    ui->setupUi(this);
    escalaActual = 1;
    pasoZoom = 25;
    QString label;
    ui->labelZoomPercent->setText(label.sprintf("%1.2f %%", escalaActual * 100.0));
    ui->entryZoomStep->setValidator( new QDoubleValidator(0, 100, 2, this) );
    ui->entryZoomStep->setText("25.0");
    setVisibleVistaPrevia(false);
    ubicacionActual = QDesktopServices::storageLocation(
                          QDesktopServices::PicturesLocation);
}

ROISelectionDialog::ROISelectionDialog(QWidget* parent) :
    QDialog(parent)
{
}
ROISelectionDialog::ROISelectionDialog(QImage imagen, QString nombreImagen)
    : ROISelectionDialog(imagen)
{
    setNombreArchivo(nombreImagen);
}

ROISelectionDialog::ROISelectionDialog(QImage imagen) : ROISelectionDialog()
{
    QImage* imagenActual = &imagen;

    if ( !imagen.byteCount() || !imagen.width() || !imagen.height() )
    {
        nombreOriginal = QFileDialog::getOpenFileName(this,
                         tr("Abrir imagen"), ubicacionActual,
                         tr("Image Files (*.png *.jpg *.bmp)"));

        if ( !nombreOriginal.length() )
        {
            return;
        }

        imagenActual = new QImage(nombreOriginal);
    }

    setImagen(*imagenActual);
    conectarElementos();
}
void ROISelectionDialog::conectarElementos()
{
    connect(elementoPixmap, SIGNAL(cuadroAgregado(QGraphicsItemPort*)), this,
            SLOT(guardarReferencias(QGraphicsItemPort*)));
    connect(elementoPixmap, SIGNAL(areaSeleccionada()), this, SLOT(actualizarGui()));
    connect(ui->buttonZoomIn, SIGNAL(clicked()), this,
            SLOT(actualizarTagAmpliacion()));
    connect(ui->buttonZoomOut, SIGNAL(clicked()), this,
            SLOT(actualizarTagAmpliacion()));
    connect(this, SIGNAL(eliminarCuadroDeEscena(QGraphicsItemPort*)),
            elementoPixmap, SLOT(eliminarCuadro(QGraphicsItemPort*)));
    ui->buttonGenerate->setEnabled(false);
    ui->buttonDeleteArea->setEnabled(false);
}

void ROISelectionDialog::setImagen(QImage imagen)
{
    pixmapOriginal = QPixmap::fromImage(imagen);
    areaDeImagen = new QGraphicsScene(this);
    ui->graphicView->setScene(areaDeImagen);
    elementoPixmap = new QGraphicsPixmapItemPort(this, pixmapOriginal);
    areaDeImagen->addItem(elementoPixmap);
    listaDeEtiquetas = NULL;
    listaDePixmap = NULL;
}

bool ROISelectionDialog::refrescarMiniaturas()
{
    limpiarListaEtiquetasPixmap();
    listaDePixmap = elementoPixmap->getListaPixmap(pixmapOriginal);
    crearEtiquetasPixmap(listaDePixmap);

    foreach (QLabel* pm, *listaDeEtiquetas)
    {
        ui->horizontalLayout->addWidget(pm);
    }

    return true;
}

void ROISelectionDialog::refrescarVistaPrevia()
{
    if ( !listaDePixmap->count() )
    {
        setVisibleVistaPrevia(false);
        return;
    }

    QList<QRgb>* listaRgb = getListaRgb(listaDePixmap);
    int* wh = calcularDimensionesDeImagen(listaRgb->count());

    if ( ultimaImagenPatron )
    {
        delete ultimaImagenPatron;
    }

    ultimaImagenPatron = getImagenPatron(*listaRgb, wh[0], wh[1]);
    setVistaPrevia(QPixmap::fromImage(*ultimaImagenPatron), wh[0],
                   wh[1], listaRgb->count());
    delete[] wh;
    delete listaRgb;
}

void ROISelectionDialog::guardarReferencias(QGraphicsItemPort* nuevoElemento)
{
    QListWidgetItem* e = new QListWidgetItem;
    e->setText( nuevoElemento->getEtiqueta() );
    e->setData(Qt::UserRole, qVariantFromValue((void*)nuevoElemento));
    ui->areaList->addItem( e );
    limpiarListaPixmap();
    refrescarMiniaturas();
    refrescarVistaPrevia();
}
void ROISelectionDialog::setVisibleVistaPrevia(bool visible)
{
    ui->labelPatternImage->setVisible(visible);
    ui->labelWidth->setVisible(visible);
    ui->labelWidthPreview->setVisible(visible);
    ui->labelHeight->setVisible(visible);
    ui->labelHeightPreview->setVisible(visible);
    ui->labelPixels->setVisible(visible);
    ui->labelPixelsPreview->setVisible(visible);
}

void ROISelectionDialog::setVistaPrevia(QPixmap pm, int ancho, int alto,
                                        int noPixeles)
{
    QString label;
    ui->labelPatternImage->setPixmap(pm);
    ui->labelWidthPreview->setText(label.sprintf("%d px", ancho));
    ui->labelHeightPreview->setText(label.sprintf("%d px", alto));
    ui->labelPixelsPreview->setText(label.sprintf("%d", noPixeles));
    setVisibleVistaPrevia(true);
}

void ROISelectionDialog::limpiarListaEtiquetasPixmap()
{
    if (listaDeEtiquetas && listaDeEtiquetas->length())
    {
        foreach (QLabel* l, *listaDeEtiquetas)
        {
            ui->horizontalLayout->removeWidget(l);
            delete l;
        }

        listaDeEtiquetas = NULL;
    }
}

void ROISelectionDialog::limpiarListaPixmap()
{
    if (listaDePixmap && listaDePixmap->length())
    {
        foreach (QPixmap* pm, *listaDePixmap)
        {
            delete pm;
        }

        listaDePixmap = NULL;
    }
}

void ROISelectionDialog::limpiarVistaPrevia()
{
    if (ui->labelPatternImage->pixmap())
    {
        ui->labelPatternImage->clear();
        setVisibleVistaPrevia(false);
    }
}
ROISelectionDialog::~ROISelectionDialog()
{
    delete ui;
}

void ROISelectionDialog::setNombreArchivo(QString nombre)
{
    nombreOriginal = nombre;
}

int* ROISelectionDialog::calcularDimensionesDeImagen(int longitud)
{
    double sr = sqrt(longitud / 1.0);
    double floor_val = floor(sr);
    double ceil_val = ceil(sr);
    double bottom = longitud - pow(floor_val, 2.0);
    double top = pow(ceil(sr), 2.0) - longitud;
    double rows, cols;

    if ( bottom < top )
    {
        rows = floor_val;
        cols = rows + ceil(bottom / rows);
    }

    else
    {
        rows = cols = ceil_val;
    }

    int* wh = new int[2];
    wh[0] = (int) cols;
    wh[1] = (int) rows;
    return wh;
}

QList<QRgb>* ROISelectionDialog::getListaRgb(QList<QPixmap*>* listaPixmap)
{
    QList<QRgb>* listaRgb = new QList<QRgb>();

    foreach (QPixmap* pm, *listaPixmap)
    {
        QImage imagen = pm->toImage();

        for (int i = 0; i < imagen.width(); ++i)
        {
            for (int j = 0; j < imagen.height(); ++j)
            {
                listaRgb->append(imagen.pixel(i, j));
            }
        }
    }

    return listaRgb;
}

QImage* ROISelectionDialog::getImagenPatron(QList<QRgb> listaPixel, int ancho,
        int alto)
{
    QImage* imagen = new QImage(ancho, alto, QImage::Format_ARGB32);
    int delta = ancho * alto - listaPixel.count();
    int b_i = 0, b_j = 0, j, i;

    for (i = 0; (i * ancho + j < delta) && i < imagen->width(); ++i)
    {
        for (j = 0; (i * ancho + j < delta) && j < imagen->height(); ++j)
        {
            imagen->setPixel(i, j, listaPixel.at(i * ancho + j));

            if ( (i * ancho + j < delta) == listaPixel.count() - 1)
            {
                b_i = i;
                b_j = j;
            }
        }
    }

    int k = 0;

    for (i = b_i; i < imagen->width(); ++i)
    {
        for (j = b_j + 1; j < imagen->height(); ++j)
        {
            imagen->setPixel(i, j, listaPixel.at(k++));
        }
    }

    return imagen;
}

void ROISelectionDialog::on_buttonGenerate_clicked()
{
    std::stringstream ss;
    QString msg;
    nombreImagenGenerada = getNombreNuevo(nombreOriginal);
    ss << "Imagen generada de " << ultimaImagenPatron->width()
       << "x" << ultimaImagenPatron->height() << " pixeles";
    emit mostrarImagen(*ultimaImagenPatron, nombreImagenGenerada);
    msg = QString::fromStdString(ss.str());
    emit setStatus(msg, 1);
}

QString ROISelectionDialog::getNombreNuevo(QString nombreImagen)
{
    QFileInfo fi(nombreImagen);
    QString sufijo = "." + fi.suffix();
    QString nombreBase = fi.baseName();
    QString rutaDir = fi.absolutePath();
    return rutaDir + "/" + prefijo + nombreBase + sufijo;
}

void ROISelectionDialog::on_buttonSave_clicked()
{
    nombreImagenGenerada = getNombreNuevo(nombreOriginal);
    QString nombreArchivo = QFileDialog::getSaveFileName(
                                this, tr("Guardar Como"), nombreImagenGenerada,
                                filter);

    if ( nombreArchivo.isEmpty() )
    {
        return;
    }

    ultimaImagenPatron->save(nombreArchivo);
    emit log(QString("").append("Imagen guardada en ").append(nombreArchivo));
}

void ROISelectionDialog::crearEtiquetasPixmap(QList<QPixmap*>* listaPixmap)
{
    QLabel* label = NULL;
    listaDeEtiquetas = new QList<QLabel*>();

    foreach (QPixmap* pm, *listaPixmap)
    {
        label = new QLabel();
        label->setGeometry(0, 0, 50, 50);
        label->setPixmap(pm->scaled(50, 50, Qt::KeepAspectRatio,
                                    Qt::FastTransformation));
        label->setScaledContents(true);
        listaDeEtiquetas->append(label);
    }
}

void ROISelectionDialog::on_buttonCancel_clicked()
{
    close();
}

void ROISelectionDialog::actualizarGui()
{
    ui->buttonGenerate->setEnabled(true);
    ui->buttonDeleteArea->setEnabled(true);
}

void ROISelectionDialog::on_buttonDeleteArea_clicked()
{
    QList<QListWidgetItem*> elementos = ui->areaList->selectedItems();

    if ( elementos.length() > 0 )
    {
        foreach (QListWidgetItem* e, elementos)
        {
            QGraphicsItemPort* cuadro = (QGraphicsItemPort*)e->data(
                                            Qt::UserRole ).value<void*>();
            ui->areaList->removeItemWidget(e);
            delete e;
            emit eliminarCuadroDeEscena(cuadro);
        }

        QString nuevoTag;
        int reenum = 0;
        QListWidgetItem* nuevoElemento = NULL;

        for (int i = 0; i < ui->areaList->count(); ++i)
        {
            nuevoElemento = ui->areaList->item(i);
            QGraphicsItemPort* box = (QGraphicsItemPort*)nuevoElemento->data(
                                         Qt::UserRole ).value<void*>();
            nuevoTag = QObject::tr("%1").arg(++reenum);
            box->setEtiqueta(nuevoTag);
            nuevoElemento->setText(nuevoTag);
        }

        refrescarMiniaturas();
        refrescarVistaPrevia();
    }

    else
    {
        elementoPixmap->limpiarCuadros();
        ui->areaList->clear();
        limpiarListaEtiquetasPixmap();
        limpiarListaPixmap();
        limpiarVistaPrevia();
        ui->buttonGenerate->setEnabled(false);
        ui->buttonDeleteArea->setEnabled(false);
    }
}

void ROISelectionDialog::on_buttonZoomIn_clicked()
{
    qreal paso = 1 + pasoZoom / 100.0;
    escalaActual *= paso;
    ui->graphicView->scale(paso, paso);
}

void ROISelectionDialog::on_buttonZoomOut_clicked()
{
    qreal paso = 1 + pasoZoom / 100.0;
    escalaActual /= paso;
    ui->graphicView->scale(1.0 / paso, 1.0 / paso);
}

void ROISelectionDialog::actualizarTagAmpliacion()
{
    QString label;
    ui->labelZoomPercent->setText(label.sprintf("%1.2f %%", escalaActual * 100.0));
}

void ROISelectionDialog::on_buttonSelectArea_toggled(bool checked)
{
    elementoPixmap->setSeleccion(checked);
}

void ROISelectionDialog::on_areaList_currentRowChanged(int currentRow)
{
    for (int j = 0; j < ui->areaList->count(); ++j)
    {
        QListWidgetItem* elemento = ui->areaList->item(j);
        QGraphicsItem* cuadro = (QGraphicsItem*)elemento->data(
                                    Qt::UserRole ).value<void*>();
        cuadro->setSelected( j == currentRow );
        elemento->setSelected( j == currentRow );
    }
}

void ROISelectionDialog::on_entryZoomStep_editingFinished()
{
    pasoZoom = ui->entryZoomStep->text().toDouble();

    if ( pasoZoom <= 0 )
    {
        ui->entryZoomStep->setText("25.0");
    }
}

QString ROISelectionDialog::getNombreImagenPatron()
{
    return nombreImagenGenerada;
}
QImage ROISelectionDialog::getImagenPatron()
{
    return imagenGenerada;
}

void ROISelectionDialog::seleccionarRois(QImage imagen)
{
    seleccionarRois(imagen, QString::fromStdString(""));
}

void ROISelectionDialog::seleccionarRois(QImage imagen,
        QString nombre)
{
    if ( !imagen.byteCount() || !imagen.width() || !imagen.height() )
    {
        QMessageBox::warning(
            0,
            QString::fromStdString("No se puede continuar"),
            QString::fromStdString("Verifique que tenga una imagen abierta"));
        return;
    }

    setImagen(imagen);
    setNombreArchivo(nombre);
    conectarElementos();
    setVisible(true);
}
Q_EXPORT_PLUGIN2(patternimagegenerator, ROISelectionDialog)
