TEMPLATE     = lib
CONFIG      += plugin
DESTDIR      = ../plugins/PatternImageGenerator
TARGET       = $$qtLibraryTarget(PatternImageGenerator)
INCLUDEPATH  = .
SOURCES      = ROISelectionDialog.cpp \
               QGraphicsItemPort.cpp \
               QGraphicsPixmapItemPort.cpp

HEADERS      = ROISelectionDialog.h \
               QGraphicsItemPort.h \
               QGraphicsPixmapItemPort.h
# install
target.path = ../
sources.files = $$SOURCES $$HEADERS $$RESOURCES $$FORMS PatternImageGenerator.pro
sources.path = .
INSTALLS += target sources
QMAKE_CXXFLAGS += -std=c++11
FORMS += ROISelectionDialog.ui

RESOURCES += \
    res.qrc

unix {
    QMAKE_POST_LINK += $$quote(cp $$PWD/PatternImageGenerator.json $$DESTDIR)
}
win32 {
    QMAKE_POST_LINK += $$quote(copy /y $$PWD/PatternImageGenerator.json $$DESTDIR)
}
