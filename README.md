
Cimahis Plugins
===============

This repository keeps plugins developed for Cimahis application.

Available plugins
-----------------
||
|------------------------|-----------------|-:|:-:|:-:|
| **Plugin Name**        | **Description** | **Version** | **Explore** | **Download** |
| SamplePlugin | Sample plugin to test the extensibility | `1.0.0` | [![](https://bitbucket.org/cimahis/cimahis-plugins/downloads/fd.png)](SamplePlugin/) | [![](https://bitbucket.org/cimahis/cimahis-plugins/downloads/dl.png)](https://bitbucket.org/cimahis/cimahis-plugins/downloads/SamplePlugin-1.0.0.zip) |
| PatternImageGenerator | Based on ROIs it generates pattern images | `1.0.0` | [![](http://findicons.com/files/icons/1389/g5_system/32/toolbar_find.png)](PatternImageGenerator/) | [![](https://bitbucket.org/cimahis/cimahis-plugins/downloads/dl.png)](https://bitbucket.org/cimahis/cimahis-plugins/downloads/PatternImageGenerator-1.0.0.zip) |
