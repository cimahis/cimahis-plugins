
Cimahis Plugins
===============


To start working with plugins, you can count with a Qt Creator wizard template to quickly create a plugin with all the files required for it.

Wizard template
---------------

### Install it
See the command sequence below and apply it.
```bash
user@ubuntu ~/$ git clone git@bitbucket.org:cimahis/developer-tools.git
user@ubuntu ~/$ cd developer-tools/templates
user@ubuntu ~/developer-tools/templates$ sudo ./install-wizards.sh
[sudo] password for user:
Installing CimahisPluginTemplate...OK
user@ubuntu ~/developer-tools/templates$
```
### Usage

Run Qt Creator, create a new project clicking on ![](./.images/00_new_project.png) and next follow the images below:

#### Go to **`Custom Projects`** and select **`Cimahis Plugin Template`**, then click **`Choose...`** button
![](./.images/01_new_project.png)

#### Name your project, and give a directory for it (e.g. `GreatPlugin`), then click in **`Next >`**.
![](./.images/02_location.png)

#### Leave all fields, and just click **`Next >`**.
![](./.images/03_kits.png)

#### Set the details for your plugins:

##### Fields explanation
 - `Debug my plugin with a main.cpp` : It will modify **`CONFIG`** and **`SOURCES`** directives in your `.pro` file, enabling execute your plugin using a `main.cpp`
 - `When building, install directly in` : If a folder (e.g. `/your/folder`) is choosen, library and manifest files will be included inside `/your/folder/plugins/GreatPlugin` after each build is done
 - `Main class name` : Give a main class name to inherit interfaces needed to your plugin.
 - `Plugin version` : Initial version for your plugin.
 - `Release Date` : Set a release date for your plugin.
 - `Plugin description` : What your plugin does? What functionalities contains? those are questions this fields may respond to.
 - `Repository URL` : If your plugin is going to be located in a repository, give a URl to be located.
 - `Your Name` : Maintainer's name responsible to keep the plugin up-to date.
 - `Your Email` : Maintainer's email.

![](./.images/04_details.png)

#### Configure Project Management and then click **`Finish`**.
![](./.images/05_summary.png)


Module directory structure
--------------------------

The module directory structure is quite simple.

![](./.images/06_project_created.png)

Those files represented above it must be the minimal files a plugin should have:

 **`GreatPlugin.json`** is the Manifest File, is it intended to have all the metadata information about plugins, it'll be explained more later in this document.

 **`GreatPlugin.pro`** is the Qt Project file, it must have Qt directives to compile.

 **`MyFirstPlugin.h` : ** Header class definition for **`GreatPlugin`**.

 **`MyFirstPlugin.cpp` : ** Source class implementation for **`GreatPlugin`**.

#### Note
 > - Both the manifest file and the shared object file must be named the same, except for the file extension.
 > - All plugins files should be contained into **`GreatPlugin/`** directory as indicated at first.
 > - `*.pro.user` configuration files must not be added in the repository, `git` ignores it and it won't be visible, so you don't have to worry about at all.
 > - `*.so` libraries files must not be added in the repository, `git` ignores it and it won't be visible, so you don't have to worry about at all.

### Manifest file

Manifest file is based is a JSON (JavaScript Object Notation) structured file which *should* have the following string keys in the main JSON Object.

 **`name`** : *Required string*, Is the plugin name, it is used as plugin identifier inside the application.

 **`version`** : *Required string*, Is the plugin version, some internal validation are based on value.

 **`release_date`** : *Optional string*, is the date when the plugin was published.

 **`description`** : *Required string*, It the description for what the plugin does, and what offers to the end-user.

 **`website`** : *Required string*, It the developer team belongs to, this is required but there is not validation over it, just know who developers are.

 **`developers`** : *Required object array*, At least one developer must be the maintainer for the plugin, this is required but there is not validation over it, just in case of issues it's helpful to report issues. It contains the followings strings:

  - **`full_name`** : plugin developer's name.
  - **`email`** : plugin developer's email.

 **`menu_actions`** : *Required object array*,  Here you can define all menu actions the application it will render as `QActions` menus along the main `QMenuBar` inside the application,

  - **`menu`** : Menu path to get the action registered inside the application's menu bar.
  - **`action`** : It is the static method that it'll be called when the menu receives Qt's signal `clicked()`.

The following is an example how the Manifest File named **`GreatPlugin.json`** for the example plugin should looks like:

```json
{
  "name": "GreatPlugin",
  "version": "0.0.1",
  "release_date": "2016-11-16",
  "description": "Just a great plugin with great functionalities",
  "website": "http://bitbucket.org/cimahis/cimahis-plugins",
  "developers": [
    {
      "full_name": "Osval Reyes",
      "email": "oreyes1@uc.edu.ve"
    }
  ],
  "menu_actions": [
    {
      "menu": "/Plugins/Ejemplo de GreatPlugin",
      "action": "menuAction"
    }
  ]
}
```
