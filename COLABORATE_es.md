
Repositorio de Plugins de Cimahis
=================================

Para empezar a trabajar con plugins, puedes contar con la plantilla para el asistente de Qt Creator para rápidamente crear un plugin con todos los archivos requeridos.

Plantilla del Asistente
-----------------------

### Instalación
Observa la secuencia de comandos, y aplicalos como se muestran a continuación:
```bash
user@ubuntu ~/$ git clone git@bitbucket.org:cimahis/developer-tools.git
user@ubuntu ~/$ cd developer-tools/templates
user@ubuntu ~/developer-tools/templates$ sudo ./install-wizards.sh
[sudo] password for user:
Installing CimahisPluginTemplate...OK
user@ubuntu ~/developer-tools/templates$
```
### Uso
Ejecuta Qt Creator, crea un nuevo proyecto haciendo click en ![](./.images/00_new_project.png), y luego sigue las imagenes a continuación:

#### Ir a **`Custom Projects`** y selecciona **`Cimahis Plugin Template`**, haz click en el botón **`Choose...`**.
![](./.images/01_new_project.png)

#### Introduce un nombre para el proyecto, un directorio para ello (por ejemplo: `GreatPlugin`), luego haz click en **`Next >`**.
![](./.images/02_location.png)

#### Puedes dejar todos los campos como se encuentran, luego haz click en **`Next >`**.
![](./.images/03_kits.png)

#### Establece los detalles para tu plugins:

##### Explicación de los campos

 - `Debug my plugin with a main.cpp` : Modificará las directivas **`CONFIG`** y **`SOURCES`** en el archivo `.pro`, habilitando ejecutar el plugin usando con un archivo fuente `main.cpp`.

 - `When building, install directly in` : Si es establecido (por ejemplo `/una/carpeta`), los archivos de la librería y manifiesto serán incluídos dentro de `/una/carpeta/plugins/GreatPlugin` luego que cada construcción del plugin.
 - `Main class name` : Introduce un nombre de clase para heredar las interfaces para el plugin.
 - `Plugin version` : Versión inicial del plugin.
 - `Release Date` : Establece la fecha de publicación del plugin.
 - `Plugin description` : ¿Que hace el plugin? ¿Que funcionalidades contiene?, estas preguntas son las que este campo debe responder.
 - `Repository URL` : Si el plugin será mantenido en un repositorio, este campo es para la direccion web del repositorio.
 - `Your Name` : Nombre del mantenedor del plugin, responsable de mantener el plugin al día.
 - `Your Email` : Correo del mantenedor.

![](./.images/04_details.png)

#### Configura el plugin bajo un control de versiones, y luego haz click en **`Finish`**.
![](./.images/05_summary.png)


Estructura de directorios de un módulo
--------------------------------------

La estructura de directorio es bastante sencilla, vamos a ver como se ve,

![](./.images/06_project_created.png)

Estos archivos representados anteriormente deben ser los archivos mínimos que un plugin debería tener:

 **`GreatPlugin.json`** Es el archivo manifiesto, está hecho para contener toda la informacion relevante sobre el plugin, que serán explicado en este documento más adelante.

 **`GreatPlugin.pro`** Es el archivo proyecto de Qt, debe tener las directivas Qt a compilar.

 **`MyFirstPlugin.h` : ** Definición de la cabecera de la clase **`GreatPlugin`**.

 **`MyFirstPlugin.cpp` : ** Código de implementación de la clase **`GreatPlugin`**.

#### Nota
> - Tanto el archivo manifiesto como como la librería del plugin deben llamarse igual, exceptuando la extensión del archivo.
> - Todos los archivos del plugin deben estar contenidos dentro del directorio **`GreatPlugin/`** indicado al principio.
> - Los archivos `*.pro.user` no deben ser añadidos al repositorio, `git` los ignora y no son visibles, por lo que no hay que preocuparse por eso.
> - Las librerías `*.so` no deben ser añadidas al repositorio, `git` los ignora y no son visibles, por lo que no hay que preocuparse por eso.

### Archivo Manifiesto

El archivo manifiesto es un archivo JSON (JavaScript Object Notation) estructurado, el cual *DEBERÍA* contener las siguientes claves de cadena en el objeto JSON principal.

 **`name`** : *Cadena requerida*, Es el nombre del plugin es usado como identificador del plugin dentro de la aplicación

 **`version`** : *Cadena requerida*, Es la versión del plugin, algunas validaciones internas en la aplicación están basadas en este valor.

 **`release_date`** : *Cadena opcional*, es la fecha que indica la fecha de publicación del plugin.

 **`description`** : *Cadena requerida*, Es la descripcion para indicar lo que hace el plugin, y que ofrece al usuario final.

 **`website`** : *Cadena requerida*, Es donde los desarrolladores pertenecen, Estos es requerido pero no existe validación alguna para ello, es sólo para saber quienes son los desarrolladores.

 **`developers`** : *Arreglo de Objetos requerido*, Al menos un desarrolladore DEBE ser el mantenedor para el plugin, Esto es requerido pero no existe validación alguna para ello, en caso de incidencias es útil para reportarlos. Contiene las siguientes cadenas:

  - **`full_name`** : Nombre del desarrollador del plugin.
  - **`email`** : Correo electrónico del plugin.

 **`menu_actions`** : *Arreglo de Objetos requerido*, En esta sección se puede definir todos los menús que la aplicación renderizará como acciones `QAction` en todo el `QMenuBar` dentro de la aplicación.

  - **`menu`** : Indica la ruta del menú para inscribir el menú en la barra de aplicación de la aplicación
  - **`action`** : Indica el método estático dentro de la clase que será llamado cuando el menu reciba la señal de Qt `clicked()`

Lo siguiente es un ejemplo para un archivo de manifiesto llamado **`GreatPlugin.json`**, debe parecerse como a continuación:

```json
{
  "name": "GreatPlugin",
  "version": "0.0.1",
  "release_date": "2016-11-16",
  "description": "Un gran plugin con grandes funcionalidades",
  "website": "http://bitbucket.org/cimahis/cimahis-plugins",
  "developers": [
    {
      "full_name": "Osval Reyes",
      "email": "oreyes1@uc.edu.ve"
    }
  ],
  "menu_actions": [
    {
      "menu": "/Plugins/Ejemplo de GreatPlugin",
      "action": "menuAction"
    }
  ]
}
```
