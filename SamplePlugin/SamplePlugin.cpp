#include "SamplePlugin.h"

SamplePlugin::SamplePlugin()
{
}

void SamplePlugin::write_on_status_bar()
{
    emit setStatus("Esto es un mensaje en la barra de estado de Cimahis ", 5);
}

void SamplePlugin::sample_plugin_message()
{
    QMessageBox::warning(0,QString::fromStdString("Mensaje en Cimahis"),
        QString::fromStdString("Esto es un cuadro de mensaje en Cimahis"));
}

void SamplePlugin::log_in_console()
{
    emit log("Esta es una salida por consola en Cimahis");
}

Q_EXPORT_PLUGIN2(SamplePlugin,SamplePlugin)
