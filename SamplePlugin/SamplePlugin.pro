TEMPLATE     = lib
CONFIG      += plugin
DESTDIR      = ../plugins/SamplePlugin
TARGET       = SamplePlugin
INCLUDEPATH  = .
SOURCES      = SamplePlugin.cpp
HEADERS      = SamplePlugin.h
OTHER_FILES  = SamplePlugin.json
# install
target.path = ../
sources.files = $$SOURCES $$HEADERS $$RESOURCES $$FORMS SamplePlugin.pro
sources.path = ../
INSTALLS += target sources

unix {
    QMAKE_POST_LINK += $$quote(cp $$PWD/SamplePlugin.json $$DESTDIR)
}
win32 {
    QMAKE_POST_LINK += $$quote(copy /y $$PWD/SamplePlugin.json $$DESTDIR)
}
