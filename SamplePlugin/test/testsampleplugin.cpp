#ifndef TESTSAMPLEPLUGIN_H
#define TESTSAMPLEPLUGIN_H

#include <QObject>
#include <QStatusBar>
#include <QtTest>
#include <QDebug>
#include <QRegExpValidator>
#include <QValidator>

#include "CData.h"
#include "SamplePlugin.h"

class TestSamplePlugin : public QObject
{
        Q_OBJECT
    private:
        CData* data;
        SamplePlugin* sp;

    private slots:
        void initTestCase();
        void testStatusBar();
        void cleanupTestCase();
};

void TestSamplePlugin::testStatusBar()
{
    int pos = 0;
    QRegExp regex(".*just deal with it!.*");
    QRegExpValidator v(regex, 0);
    sp->write_on_status_bar(data);
    QString message = data->statusBar->currentMessage();
    QCOMPARE(v.validate(message, pos),  QValidator::Acceptable);
}


void TestSamplePlugin::initTestCase()
{
    this->data = new CData();
    data->statusBar = new QStatusBar();
    sp = new SamplePlugin();
}

void TestSamplePlugin::cleanupTestCase()
{
    delete sp;
    delete data->statusBar;
    delete data;
}

#include "testsampleplugin.moc"
QTEST_MAIN(TestSamplePlugin)

#endif // TESTSAMPLEPLUGIN_H
