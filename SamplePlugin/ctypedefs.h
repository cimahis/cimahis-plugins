#ifndef CTYPESDEFS_H
#define CTYPESDEFS_H

#include <map>
#include <string>

#include <QObject>
#include <QMap>
#include <QAction>

// forward declaration
class CPluginData;


/* Plugin's properties in manifest file storage for all metadata that provides
 * its description
 */
typedef std::pair<std::string, std::string> DataPair;
typedef std::map<std::string, std::string> PluginMetadata;

/*
 * map: key: plugin name
 *      val: plugin data
 */
typedef std::map<std::string, CPluginData *> PluginMap;
typedef PluginMap::iterator MapIt;
typedef std::pair<std::string, CPluginData *> PluginMapPair;

/*
 * map: key: plugin method name
 *      val: plugin pointer
 */
typedef std::map<std::string, QObject *> PointerMap;
typedef std::pair<std::string, QObject *> PointerMapPair;


/*
 *
 *
 */
typedef QMap<QString, QAction *> PluginActionMap;
#endif // CTYPEDEFS_H
