#ifndef SAMPLEPLUGIN_H
#define SAMPLEPLUGIN_H

#include <QObject>
#include <QImage>
#include <QString>
#include <QtGui>
#include "CLogInterface.h"
#include "CStatusBarInterface.h"

class SamplePlugin : public QObject, CLogInterface,
                            CStatusBarInterface
{
    Q_OBJECT
    Q_INTERFACES(CLogInterface)
    Q_INTERFACES(CStatusBarInterface)

    public:
        SamplePlugin();
    public slots:
        void write_on_status_bar();
        void sample_plugin_message();
        void log_in_console();
    signals:
        void log(QString str);
        void setStatus(QString str, int timeout);
};

#endif // SAMPLEPLUGIN_H
