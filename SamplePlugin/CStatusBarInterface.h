#ifndef CSTATUSBARINTERFACE_H
#define CSTATUSBARINTERFACE_H

#include <QString>

class CStatusBarInterface
{
signals:
    virtual void setStatus(QString str, int timeout) = 0;
public:
    virtual ~CStatusBarInterface(){}
};


Q_DECLARE_INTERFACE(CStatusBarInterface, "org.cimahis.CStatusBarInterface/1.0.0")

#endif // CSTATUSBARINTERFACE_H
