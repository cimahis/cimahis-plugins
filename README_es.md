
Plugins de Cimahis
==================

Este repositorio mantiene los plugins desarrollados para Cimahis.

Plugins Disponibles
-------------------
||
|------------------------|-----------------|-:|:-:|:-:|
| **Nombre del Plugin**  | **Descripción** | **Versión** | **Explorar** | **Descargar** |
| SamplePlugin | Plugin de Prueba para probar la extensibilidad | `1.0.0` | [![](https://bitbucket.org/cimahis/cimahis-plugins/downloads/fd.png)](SamplePlugin/) | [![](https://bitbucket.org/cimahis/cimahis-plugins/downloads/dl.png)](https://bitbucket.org/cimahis/cimahis-plugins/downloads/SamplePlugin-1.0.0.zip) |
| PatternImageGenerator | Genera imágenes patrón basadas en ROIs | `1.0.0` | [![](http://findicons.com/files/icons/1389/g5_system/32/toolbar_find.png)](PatternImageGenerator/) | [![](https://bitbucket.org/cimahis/cimahis-plugins/downloads/dl.png)](https://bitbucket.org/cimahis/cimahis-plugins/downloads/PatternImageGenerator-1.0.0.zip) |
